#!/bin/bash

#
# vNu_HTML.sh
#
# Run the Nu Html Checker (v.Nu)
# and filter some error messages on old (X)HTML.
#

if [[ -z "$1" ]]; then  # helping message
    echo 'Usage: vNu_HTML.sh FILE1 [FILE2...]'
    echo 'Run the Nu Html Checker (v.Nu)'
    echo 'and filter some error messages on old HTML (previous HTML5) or XHTML.'
    echo
    echo 'Requirements'
    echo '  Command: cat, cp, echo, expr, file, grep, head, iconv, java, sed, sort, tr'
    echo '  Java JAR: vnu.jar'
    echo '  Associated scripts: vNu.sh and which_HTML.sh'
    echo
    echo 'https://bitbucket.org/OPiMedia/vnu-scripts'
    echo 'GPLv3 --- Copyright (C) 2018, 2021'
    echo 'Olivier Pirson --- http://www.opimedia.be/'
    echo 'October 30, 2021'

    exit 1
fi


OQ=$(echo -e '\xe2\x80\x9c')  # double open quotes
CQ=$(echo -e '\xe2\x80\x9d')  # double close quotes


TABLEEXCLUDEPATTERN="The ${OQ}(align|bgcolor|border|cellpadding|cellspacing|width)${CQ} attribute on the ${OQ}table${CQ} element is obsolete. Use CSS instead.|The ${OQ}(align|bgcolor|nowrap|valign|width)${CQ} attribute on the ${OQ}(tbody|td|th|tr)${CQ} element is obsolete. Use CSS instead."

LISTEXCLUDEPATTERNS="The ${OQ}type${CQ} attribute on the ${OQ}(ol|ul)${CQ} element is obsolete. Use CSS instead."

IMGEXCLUDEPATTERN="Bad value ${OQ}[1-9]${CQ} for attribute ${OQ}border${CQ} on element ${OQ}img${CQ}: Only ${OQ}0${CQ} is a permitted zero literal.|The ${OQ}align${CQ} attribute on the ${OQ}img${CQ} element is obsolete. Use CSS instead.|The ${OQ}border${CQ} attribute is obsolete. Consider specifying ${OQ}img { border: 0; }${CQ} in CSS instead."

DIVEXCLUDEPATTERN="The ${OQ}align${CQ} attribute on the ${OQ}(div|h[1-6]|p)${CQ} element is obsolete. Use CSS instead."

ITEMEXCLUDEPATTERN="The ${OQ}acronym${CQ} element is obsolete. Use the ${OQ}abbr${CQ} element instead.|The ${OQ}(big|center|font|strike|tt)${CQ} element is obsolete. Use CSS instead.|The ${OQ}(noshade|size|width)${CQ} attribute on the ${OQ}hr${CQ} element is obsolete. Use CSS instead."

FRAMEEXCLUDEPATTERNS="Bad value ${OQ}100%${CQ} for attribute ${OQ}width${CQ} on element ${OQ}iframe${CQ}: Expected a digit but saw ${OQ}%${CQ} instead.|Text not allowed in element ${OQ}iframe${CQ} in this context.|The ${OQ}(frameborder|scrolling)${CQ} attribute on the ${OQ}iframe${CQ} element is obsolete. Use CSS instead.|The ${OQ}frameset${CQ} element is obsolete. Use the ${OQ}iframe${CQ} element and CSS instead, or use server-side includes."

HEADEXCLUDEPATTERNS="The ${OQ}type${CQ} attribute for the ${OQ}style${CQ} element is not needed and should be omitted.|The ${OQ}type${CQ} attribute is unnecessary for JavaScript resources."

DOCTYPEEXCLUDEPATTERN="Almost standards mode doctype. Expected ${OQ}<!DOCTYPE html>${CQ}.|Obsolete doctype. Expected ${OQ}<!DOCTYPE html>${CQ}."


EXCLUDEPATTERNS="($TABLEEXCLUDEPATTERN|$LISTEXCLUDEPATTERNS|$IMGEXCLUDEPATTERN|$DIVEXCLUDEPATTERN|$ITEMEXCLUDEPATTERN|$FRAMEEXCLUDEPATTERNS|$HEADEXCLUDEPATTERNS|$DOCTYPEEXCLUDEPATTERN)\$"


HTML5LIST=()
OLDHTMLLIST=()


# For each file, in the lexicographic order
FILES="$*"
FILES=$(sort -f <<< "${FILES// /$'\n'}")
FILES="${FILES//$'\n'/ }"
for FILE in $FILES; do
    if [[ -f "$FILE" ]]; then
        TYPE_ENCODING=$(file -b --mime "$FILE")
        if [[ "$TYPE_ENCODING" =~ ^'text/html;' ]]; then  # (X)HTML
            which_HTML.sh "$FILE"
            if [[ "$?" == 5 ]]; then  # HTML5
                HTML5LIST+=("$FILE")  # check after
            else                      # old HTML or XHTML
                [[ "$TYPE_ENCODING" =~ charset=(.+) ]] && ENCODING="${BASH_REMATCH[1]}"
                if [[ "$ENCODING" == 'utf-8' ]]; then  # already UTF-8
                    OLDHTMLLIST+=("$FILE")  # check after
                else                                   # convert to UTF-8, change charset, valid now and filter error messages
                    iconv -f "$ENCODING" "$FILE" -t UTF-8 \
                        | sed -E "0,/^\\s*<meta/ s#^(\\s*<meta http-equiv=\"content-type\" content=\"text/html; charset=)(ISO-8859-1|$ENCODING)(\"\\s*/?>)#\\1UTF-8\\3#i" \
                        | vNu.sh - 2>&1 \
                        | grep -E -v "$EXCLUDEPATTERNS" \
                        | sed -E "s|(.+)|$FILE OLD (X)HTML CONVERTED\\1|"
                fi
            fi
        else                                              # neither HTML nor XHTML
            echo "$FILE: NOT (X)HTML"
        fi
    fi
done


if [[ ${#OLDHTMLLIST[@]} != 0 ]]; then  # check all old (X)HTML files not converted in only one call, and filter error messages
    vNu.sh "${OLDHTMLLIST[@]}" 2>&1 \
        | grep -E -v "$EXCLUDEPATTERNS" \
        | sed -E "s|(\".+?\")(.+)|\\1 OLD (X)HTML\\2|"
fi


if [[ ${#HTML5LIST[@]} != 0 ]]; then  # check all HTML5 files in only one call
    vNu.sh "${HTML5LIST[@]}" 2>&1
fi
