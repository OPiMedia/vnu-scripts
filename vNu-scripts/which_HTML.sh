#!/bin/bash

#
# which_HTML.sh
#
# Determine if a file is a (X)HTML file and its version.
#

if [[ -z "$1" || -n "$2" ]]; then  # helping message
    echo 'Usage: which_HTML.sh FILE'
    echo 'Determine if FILE is a (X)HTML file and its version.'
    echo 'Return 0 if not (X)HTML or if (X)HTML not correctly identified'
    echo '       4 if HTML 4 is identified'
    echo '       5 if HTML 5 is identified'
    echo '     255 if XHTML 1 is identified (255 = -1 on 8 bits)'
    echo
    echo 'Requirements'
    echo '  Commands: echo, file and head'
    echo
    echo 'https://bitbucket.org/OPiMedia/vnu-scripts'
    echo 'GPLv3 --- Copyright (C) 2018, 2021'
    echo 'Olivier Pirson --- http://www.opimedia.be/'
    echo 'October 30, 2021'

    exit 1
fi



shopt -s nocasematch

TYPE=$(file -b --mime-type "$1")
if [[ "$TYPE" == 'text/html' ]]; then  # (X)HTML
    FIRSTLINE=$(head -n 1 "$1")

    if [[ "$FIRSTLINE" =~ '<!DOCTYPE html>' ]]; then
        # HTML5
        exit 5
    else
        if [[ "$FIRSTLINE" =~ '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4' ]]; then
            # HTML 4 as "<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">"
            exit 4
        else
            if [[ "$FIRSTLINE" =~ '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1' ]]; then
                # XHTML 1 as "<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">"
                exit -1
            else
                # (X)HTML not correctly identified
                exit 0
            fi
        fi
    fi
else                                   # neither HTML nor XHTML
    exit 0
fi
