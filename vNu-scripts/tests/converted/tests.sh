#!/bin/sh

#
# test/tests.sh
#
# https://bitbucket.org/OPiMedia/vnu-scripts
# GPLv3 --- Copyright (C) 2018
# Olivier Pirson --- http://www.opimedia.be/
# November 23, 2018
#

export PATH=../:$PATH

FILES=$(echo ./* | sed 's/correct_tests.log//' | tr ' ' '\n' | sort -f | tr '\n' ' ')



echo '===== Test which_HTML.sh ====='
which_HTML.sh
echo

for FILE in $FILES
do
    which_HTML.sh "$FILE"
    printf "which_HTML.sh \"%s\"\t--> %i\n" "$FILE" $?
done



printf '\n===== Test iconv_HTML_to_UTF-8.sh =====\n'
iconv_HTML_to_UTF-8.sh

for FILE in $FILES
do
    if [ -f "$FILE" ]
    then
        echo

        iconv_HTML_to_UTF-8.sh "$FILE" "converted/$FILE"
        printf "iconv_HTML_to_UTF-8.sh \"%s\" \"converted/$FILE\"\t--> %i\n" "$FILE" $?

        iconv_HTML_to_UTF-8.sh "$FILE" > "redirected/$FILE"
        printf "iconv_HTML_to_UTF-8.sh \"%s\" > \"redirected/$FILE\"\t --> %i\n" "$FILE" $?

        DIFF=$(cmp -b "converted/$FILE" "redirected/$FILE" 2>&1)
        if [ -n "$DIFF" ]
        then
            echo "cmp -b \"converted/$FILE\" \"redirected/$FILE\" 2>&1"
            echo "$DIFF"
        fi

        DIFF=$(cmp -b "$FILE" "converted/$FILE" 2>&1)
        if [ -n "$DIFF" ]
        then
            echo "cmp -b \"$FILE\" \"converted/$FILE\" 2>&1"
            echo "$DIFF"
        fi
    fi
done



printf '\n===== Test vNu.sh =====\n'
vNu.sh 2>&1

echo
echo 'vNu.sh --version'
vNu.sh --version 2>&1

echo
vNu.sh ./*.html 2>&1

echo
vNu.sh --css ./*.css 2>&1



printf '\n===== Test vNu_HTML.sh =====\n'
vNu_HTML.sh

echo
vNu_HTML.sh "$FILES"
