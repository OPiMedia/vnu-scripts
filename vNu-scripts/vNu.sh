#!/bin/bash

#
# vNu.sh
#
# Simply run the Nu Html Checker (v.Nu).
#

if [[ -z "$1" ]]; then  # helping message
    echo 'Usage: vNu.sh [OPTION...] FILE1 [FILE2...]'
    echo 'Simple script to run the Nu Html Checker (v.Nu).'
    echo
    echo 'See vNu options with vNu.sh --help'
    echo 'or see https://validator.github.io/validator/'
    echo
    echo 'Requirements'
    echo '  Command: java'
    echo '  Java JAR: vnu.jar (must be in the same directory that this script)'
    echo
    echo 'https://bitbucket.org/OPiMedia/vnu-scripts'
    echo 'GPLv3 --- Copyright (C) 2018, 2021'
    echo 'Olivier Pirson --- http://www.opimedia.be/'
    echo 'October 30, 2021'

    exit 1
fi


java -jar "$(dirname "$0")/vnu.jar" "$@"
