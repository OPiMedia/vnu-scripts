#!/bin/bash

#
# iconv_HTML_to_UTF-8.sh
#
# Convert a file to UTF-8 and change charset value if it is old (X)HTML.
#

if [[ -z "$1" || -n "$3" ]]; then  # helping message
    echo 'Usage: iconv_HTML_to_UTF-8.sh FILEIN [FILEOUT]'
    echo 'Convert FILEIN to UTF-8.'
    echo 'If it is a old (X)HTML file (not HTML5)'
    echo 'then replace also the first mention of charset.'
    echo
    echo '  FILEOUT: output file (instead standard output)'
    echo
    echo 'Requirements'
    echo '  Commands: cat, cp, file, head, iconv and sed'
    echo
    echo 'https://bitbucket.org/OPiMedia/vnu-scripts'
    echo 'GPLv3 --- Copyright (C) 2018, 2021'
    echo 'Olivier Pirson --- http://www.opimedia.be/'
    echo 'October 30, 2021'

    exit 1
fi



shopt -s nocasematch

# Identify encoding of source and if it is a (X)HTML file
ENCODING=$(file -b --mime-encoding "$1")

TYPE_ENCODING=$(file -b --mime "$1")
[[ "$TYPE_ENCODING" =~ text/html\; ]] && ISHTML=1
[[ "$TYPE_ENCODING" =~ charset=(.+) ]] && ENCODING="${BASH_REMATCH[1]}"


[[ "$ENCODING" != 'utf-8' && "$ENCODING" != 'us-ascii' ]] && TOBECONV=1

[[ -n "$ISHTML" && "$ENCODING" != 'utf-8' && $(head -n 1 "$1") =~ '<!DOCTYPE HTML PUBLIC "-//W3C//DTD ' ]] && TOBECHANGE=1



# Conversion and change charset (or not)
if [[ -z "$TOBECONV" ]]; then  # no conversion
    if [[ -z "$TOBECHANGE" ]]; then  # no change
        if [[ -z "$2" ]]; then  # copy to standard output
            cat "$1"
        else                    # copy to file $2
            cp "$1" "$2"
        fi
    else                             # change (X)HTML charset
        if [[ -z "$2" ]]; then  # copy to standard output
            sed -E "0,/^\\s*<meta/ s#^(\\s*<meta http-equiv=\"content-type\" content=\"text/html; charset=)(ISO-8859-1|$ENCODING)(\"\\s*/?>)#\\1UTF-8\\3#i" "$1"
        else                    # copy to file $2
            sed -E "0,/^\\s*<meta/ s#^(\\s*<meta http-equiv=\"content-type\" content=\"text/html; charset=)(ISO-8859-1|$ENCODING)(\"\\s*/?>)#\\1UTF-8\\3#i" "$1" > "$2"
        fi
    fi
else                           # convert to UTF-8
    if [[ -z "$TOBECHANGE" ]]; then  # no change
        if [[ -z "$2" ]]; then  # copy to standard output
            iconv -f "$ENCODING" "$1" -t UTF-8
        else                    # copy to file $2
            iconv -f "$ENCODING" "$1" -t UTF-8 -o "$2"
        fi
    else                             # change (X)HTML charset
        if [[ -z "$2" ]]; then  # copy to standard output
            iconv -f "$ENCODING" "$1" -t UTF-8 \
                | sed -E "0,/^\\s*<meta/ s#^(\\s*<meta http-equiv=\"content-type\" content=\"text/html; charset=)(ISO-8859-1|$ENCODING)(\"\\s*/?>)#\\1UTF-8\\3#i"
        else                    # copy to file $2
            iconv -f "$ENCODING" "$1" -t UTF-8 \
                | sed -E "0,/^\\s*<meta/ s#^(\\s*<meta http-equiv=\"content-type\" content=\"text/html; charset=)(ISO-8859-1|$ENCODING)(\"\\s*/?>)#\\1UTF-8\\3#i" > "$2"
        fi
    fi
fi
