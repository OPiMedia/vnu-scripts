# vNu-scripts
Shell scripts to run *the Nu Html Checker (v.Nu)* to valid (X)HTML files
by identifying if each file is a (X)HTML file and which version of (X)HTML.

The interest is the possibility to run like `vNu-scripts/vNu_HTML.sh *.html` or even `vNu-scripts/vNu_HTML.sh *`
with files that are HTML 5 or not.
Old HTML or XHTML files are (implicitly) converted to UTF-8
and more or less the HTML 5 news are authorized (error messages are filtered).

- [vNu-scripts/vNu_HTML.sh](https://bitbucket.org/OPiMedia/vnu-scripts/src/master/vNu-scripts/vNu_HTML.sh):
  the main script, run *the Nu Html Checker* and filter some error messages on old HTML (previous HTML5) or XHTML.
- [vNu-scripts/vNu.sh](https://bitbucket.org/OPiMedia/vnu-scripts/src/master/vNu-scripts/vNu.sh):
  run *the Nu Html Checker* (simply launch Java with the JAR file).
- [vNu-scripts/iconv_HTML_to_UTF-8.sh](https://bitbucket.org/OPiMedia/vnu-scripts/src/master/vNu-scripts/iconv_HTML_to_UTF-8.sh):
convert a file to UTF-8 and change charset value if it is old (X)HTML.
- [vNu-scripts/which_HTML.sh](https://bitbucket.org/OPiMedia/vnu-scripts/src/master/vNu-scripts/which_HTML.sh):
  determine if a file is a (X)HTML file and its version.

- [vNu-scripts/vnu.jar](https://bitbucket.org/OPiMedia/vnu-scripts/src/master/vNu-scripts/vnu.jar):
  *The Nu Html Checker (v.Nu)* version 20.6.30 from <https://validator.github.io/validator/>
  (must be included in the same directory that the script files)

- [downloads] – `vNu-scripts--with-JAR.zip`: contains the 4 script files and the JAR file
- [downloads] – `vNu-scripts--without-JAR.zip`: contains the 4 scripts files without JAR file

[downloads]: https://bitbucket.org/OPiMedia/vnu-scripts/downloads/



# Installation
1. Copy the 4 script files and the JAR file in a directory.
2. Add this directory to your PATH: `export PATH=<DIRECTORY>:$PATH`



# Requirements
- `bash` and `sh`
- Classic Unix commands: `cat`, `cp`, `echo`, `expr`, `file`, `grep`, `head`, `iconv`, `sed`, `sort` and `tr`
- Java
- `vnu.jar` (must be in the same directory than vNu.sh)



# Possible improvement
When you run `vNu-scripts/vNu_HTML.sh *`
all HTML 5 files are analysed by `vnu.jar`
in only one Java run.
All old (X)HTML files that are already in UTF-8 are analyzed
also in only one Java run.
But old (X)HTML files that must be (implicitly) converted to UTF-8
are analyzed file by file, so Java need to be run every time.
It is slow.

If you can use [Nailgun](http://www.martiansoftware.com/nailgun/)
you can used it to run several times `vnu.jar`
without run Java every time.

If you can used it, please help me. 😀
Answer to this question on StackOverlflow:
[How run a simple Java program with the client/server Nailgun (on Debian Stretch)?](https://stackoverflow.com/questions/53363931/how-run-a-simple-java-program-with-the-client-server-nailgun-on-debian-stretch)



## Author: 🌳 Olivier Pirson — OPi ![OPi][OPi] 🇧🇪🇫🇷🇬🇧 🐧 👨‍💻 👨‍🔬
🌐 Website: <http://www.opimedia.be/>

💾 Bitbucket: <https://bitbucket.org/OPiMedia/>

- 📧 <olivier.pirson.opi@gmail.com>
- 🧑‍🤝‍🧑 Mastodon: <https://mamot.fr/@OPiMedia> — Twitter: <https://twitter.com/OPirson>
- 👨‍💻 LinkedIn: https://www.linkedin.com/in/olivierpirson/ — CV: http://www.opimedia.be/CV/English.html
- other profiles: <http://www.opimedia.be/about/>

[OPi]: http://www.opimedia.be/_png/OPi.png



## License: [GPLv3](https://www.gnu.org/licenses/gpl-3.0.html) ![GPLv3](https://www.gnu.org/graphics/gplv3-88x31.png)
Copyright (C) 2018, 2021-2022 Olivier Pirson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.



![vNu-scripts](https://bitbucket-assetroot.s3.amazonaws.com/c/photos/2020/Jun/14/2524890169-5-vnu-scripts-logo_avatar.png)

Logo built with
<https://www.iconfinder.com/icons/1930264/check_complete_done_green_success_valid_icon>
and
<https://www.iconfinder.com/icons/1024958/code_coding_e_learning_e-learning_html_hyper_text_markup_language_solid_icon>



## Other personal Bash free softwares
* [etoolbox](https://bitbucket.org/OPiMedia/etoolbox/):
  Various scripts.
* [gocd (Bash)](https://bitbucket.org/OPiMedia/gocd-bash/):
  Change working directory from a list of association `NAME: DIRECTORY`, with autocompletion.
* [HackerRank [CodinGame…] / helpers](https://bitbucket.org/OPiMedia/hackerrank-codingame-helpers/):
  Help in solving problems of HackerRank website (or CodinGame…), in several programming languages.
* [proc_deleted (Bash)](https://bitbucket.org/OPiMedia/proc_deleted-bash/):
  List processes that have some deleted files, and allow to kill them.
